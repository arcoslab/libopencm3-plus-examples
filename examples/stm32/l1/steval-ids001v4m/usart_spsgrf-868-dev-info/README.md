# steval-ids001v4m/spi

This example program demonstrates:

- Reading the Device version number (reg: 0xF1) from the spsgrf-868 radio chip with the SPI2 stm32l1 peripheral.
- Send this information through the USART2 on connector J3.
- Testting LEDs
- Setting max uC frequency
